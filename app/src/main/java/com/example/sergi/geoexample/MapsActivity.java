package com.example.sergi.geoexample;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, AdapterView.OnItemSelectedListener, View.OnClickListener {

    private static final String TAG = "tag";
    private GoogleMap mMap;
    ToggleButton btnAnimacio;
    Button btnPuntInteres;
    EditText etPuntInteres;
    LatLng INS_BOSC_DE_LA_COMA = new LatLng(42.1727, 2.47631);

    DatabaseReference db;
    List<PI> puntsInteres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        configurarGUI();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        
        db = FirebaseDatabase.getInstance().getReference("points");

        puntsInteres = new ArrayList<>();

        btnPuntInteres = (Button) this.findViewById(R.id.btnPuntInteres);
        btnPuntInteres.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BuscarPuntInteres();
            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);

        //Afegir un marcador del bosc de la coma i movem la camera.
        mMap.addMarker(new MarkerOptions()
                .position(INS_BOSC_DE_LA_COMA)
                .title("Institut Bosc de la Coma")
                .snippet("Estudis: ESO, Batxillerat i Cicles Formatius")
        );

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(INS_BOSC_DE_LA_COMA, 15));

        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        mMap.getUiSettings().setCompassEnabled(true);

        mMap.getUiSettings().setZoomControlsEnabled(true);

    }

    private void AfegirPuntsInteres() {

        db.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                puntsInteres.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    PI puntInteres = postSnapshot.getValue(PI.class);
                    LatLng coords = new LatLng(puntInteres.getLatitude(), puntInteres.getLongitude());

                    mMap.addMarker(new MarkerOptions()
                    .position(coords)
                    .title(puntInteres.getName())
                    .snippet(puntInteres.getCity()));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void AfegirPuntsInteresCiutat(final String ciutat) {

        db.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                puntsInteres.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    PI puntInteres = postSnapshot.getValue(PI.class);

                    if(puntInteres.getCity().toString().equals(ciutat)) {

                        LatLng coords = new LatLng(puntInteres.getLatitude(), puntInteres.getLongitude());

                        mMap.addMarker(new MarkerOptions()
                                .position(coords)
                                .title(puntInteres.getName())
                                .snippet(puntInteres.getCity()));
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void BuscarPuntInteres(){

        etPuntInteres = (EditText) findViewById(R.id.etPuntInteres);
        String ciutat = etPuntInteres.getText().toString();
        Query query;

        if(ciutat.length() > 0){
            query = db.orderByChild("city").equalTo(ciutat);

            mMap.clear();

            LatLng latLngCiutat = GetCityLatitude(this, ciutat);
            if(latLngCiutat != null)
            {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngCiutat,13));
                AfegirPuntsInteresCiutat(ciutat);
            }
        } else {
            AfegirPuntsInteres();
        }
    }

    public static LatLng GetCityLatitude(Context mContext, String location) {
        Geocoder gc = new Geocoder(mContext);
        List<Address> addresses = null;
        LatLng latLng = null;
        try {

            addresses = gc.getFromLocationName(location, 1); // get the found Address Objects
            Address address = addresses.get(0);
            latLng = new LatLng(address.getLatitude(), address.getLongitude());


            } catch (IOException e1) {
            e1.printStackTrace();
        }

        return latLng;
    }

    public void configurarGUI() {

        Spinner cmbTipusMaps = findViewById(R.id.cmbTipusMapa);
        cmbTipusMaps.setOnItemSelectedListener(this);

        Button btnCentrar = findViewById(R.id.btnCentrar);
        btnCentrar.setOnClickListener(this);

        btnAnimacio = findViewById(R.id.tglBtnAnimacio);

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnCentrar) {
            centrarMapa();
        }

    }

    private void centrarMapa() {

        if (btnAnimacio.isChecked()) {
            mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(INS_BOSC_DE_LA_COMA, 13), 5000, null);
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    INS_BOSC_DE_LA_COMA, 13));
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        switch (i) {
            case 0:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case 1:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case 2:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case 3:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

//    @Override
//    public void onpuntInteresnterCaptureChanged(boolean hasCapture) {
//
//    }

}
